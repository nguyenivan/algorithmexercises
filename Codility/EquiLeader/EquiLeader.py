﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

from CodilityLib import Test

import Queue

import sys
def leader(A):
    n = len(A)
    size = 0
    for k in xrange(n):
        if (size == 0):
            size += 1
            value = A[k]
        else:
            if (value != A[k]):
                size -= 1
            else:
                size += 1
    candidate = -sys.maxint
    count_vector = [0] * n
    if (size > 0):
        candidate = value
    else:
        return (False, -sys.maxint, 0, count_vector)
    leader = -sys.maxint
    count = 0
    for k in xrange(n):
        if (A[k] == candidate):
            count += 1
        count_vector[k] = count
    if (count > n // 2):
        leader = candidate
        return (True, leader, count, count_vector)
    else:
        return (False, -sys.maxint, 0, count_vector)

def solution(A):
    N = len(A)
    found, l, count, count_vector = leader(A)
    if not found:
        return 0
    r = 0
    for i in xrange(N):
        left_leader =  count_vector[i] > (i+1) // 2
        right_leader = count - count_vector[i] > (N-i-1) // 2
        if left_leader and right_leader:
            r  += 1
            #print i
    return r
            

def solution_naive(A):
    N = len(A)
    f, l, c, v = leader(A)
    if not f:
        return 0
    r = 0
    for i in xrange(N):
        if len(filter(lambda x: x==l, A[:i+1])) > ((i+1)/2 ) and len(filter(lambda x: x==l, A[i+1:N])) > ((N-i-1)/2):
            r+=1
            #print i
    return r

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class CorrectnessTest(Test):
    def test_example(self):
        A=[4, 3, 4, 4, 4, 2]
        print A
        actual = solution(A)
        expected = solution_naive(A)
        self.assertEqual(actual, expected)
        pass
    def test_single(self):
        A=[5]
        print A
        actual = solution(A)
        expected = 0
        self.assertEqual(actual, expected)
        pass
    def test_double(self):
        A=[-1,-1]
        print A
        actual = solution(A)
        expected = 1
        self.assertEqual(actual, expected)
        pass
    def test_single_negative(self):
        A=[-1]
        print A
        actual = solution(A)
        expected = 0
        self.assertEqual(actual, expected)
        pass
    def test_positive_negative(self):
        A=[-1,-1,-1,10]
        print A
        actual = solution(A)
        expected = 1
        self.assertEqual(actual, expected)
        pass

class LoadTest(Test):
    def __init__(self, methodName='runTest', n=None):
        super(LoadTest, self).__init__(methodName)
        self._N = n

    def setUp(self):
        super(LoadTest,self).setUp()
        self._A = random.sample([x for x in xrange(self._N)],self._N)
        print self._N
        #print self._A
        #print self._B

    def tearDown(self):
        super(LoadTest, self).tearDown()

    def load_test(self):
        A = self._A 
        actual = solution(A)
        expected = -1
        self.assertGreater(actual, expected)
        pass

def runload():
    step = 10
    runTimeAll = []
    failTests = 0
    for i in xrange(1,10000):
        testCase = LoadTest("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def customload():
    suite = unittest.TestLoader().loadTestsFromTestCase(LoadTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)
    pass

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(CorrectnessTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)

def runone(test):
     unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(test))

if __name__ == "__main__":
    runall()

