﻿def solution(A):
    n = len(A) - 1
    s = sum(A)
    distance = [0] * n
    distance[0] = s - (2 * A[0])
    ret = abs(distance[0])
    for i in xrange(1,n):
        distance[i] = - (2 * A[i]) + distance[i - 1]
        if (abs(distance[i]) < ret):
            ret = abs(distance[i])
    return ret


import unittest
import random
import numpy as np
import os

class Test(unittest.TestCase):
    def setUp(self):
        data =  self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None


    def test_example(self):
        A = [3,1,2,4,3]
        actual = solution(A)
        expected = 1
        self.assertEqual(actual,expected)

    def test_correctness(self):
        A = [-1000,1000]
        actual = solution(A)
        expected = 2000
        self.assertEqual(actual,expected)

    def test_small_range(self):
        #A = np.loadtxt("small_range.txt", int)
        A = self._A
        actual = solution(A)
        expected = 7
        self.assertEqual(actual,expected)

    def test_medium_random1(self):
        #A = np.loadtxt("medium_random1.txt", int)
        A = self._A
        actual = solution(A)
        expected = 0
        self.assertEqual(actual,expected)

    def test_large_extreme(self):
        A = self._A
        #A = np.loadtxt("large_extreme.txt", int)
        actual = solution(A)
        expected = 0
        self.assertEqual(actual,expected)

def runtest():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(Test(s))
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity = 2).run(suite)

if __name__ == "__main__":
    runtest()