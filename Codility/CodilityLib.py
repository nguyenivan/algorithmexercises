﻿import sys
def leader(A):
    n = len(A)
    size = 0
    for k in xrange(n):
        if (size == 0):
            size += 1
            value = A[k]
        else:
            if (value != A[k]):
                size -= 1
            else:
                size += 1
    candidate = -sys.maxint
    if (size > 0):
        candidate = value
    else:
        return (False, -sys.maxint)
    leader = -sys.maxint
    count = 0
    for k in xrange(n):
        if (A[k] == candidate):
            count += 1
        if (count > n // 2):
            leader = candidate
            return (True, leader)
    return (False, -sys.maxint)

def counting(A, m): 
    N = len(A) 
    count = [0] * (m + 1) 
    for k in xrange(N): 
        count[A[k]] += 1
    return count

def mergeSort(A):
    if len(A)>1:
        mid = len(A)//2
        lefthalf = A[:mid]
        righthalf = A[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                A[k]=lefthalf[i]
                i=i+1
            else:
                A[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < len(lefthalf):
            A[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < len(righthalf):
            A[k]=righthalf[j]
            j=j+1
            k=k+1
class MyQueue:
    N = 100
    queue = [0]
    head, tail = 0, 0
    def push(x):
        global tail
        tail = (tail + 1) % N
        queue[tail] = x
    def pop():
        global head
        head = (head + 1) % N
        return queue[head]
    def size():
        return(tail - head + N) % N
    def empty():
        return head == tail

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):
    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime = self._tock - self._tick
        return runTime.microseconds / 1000