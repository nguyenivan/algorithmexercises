﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

from CodilityLib import Test

import math
def naive(A):
    # Find peak
    N = len(A)
    peak = [0] * N
    M = 0
    #print peak
    for p in xrange(1,N - 1):
        if A[p - 1] < A[p] > A[p + 1]:
            peak[M] = p
            M += 1
    #print peak
    # Find max k flags, k cannot exceed N
    max_k = min(M,1)
    for k in xrange(0,N):
        for i in xrange(0,M):
            count = 1
            p = i
            while p < M:
                q = p + 1
                while q < M:
                    if peak[q] - peak[p] >= k:
                        count += 1
                        break
                    q += 1
                p = q
            if count >= k:
                max_k = max(max_k,k)
    return max_k

    pass    

import math
def solution(A):
    N = len(A)
    peak = []
    peak_debug = []
    for p in xrange(1,N - 1):
        if A[p - 1] < A[p] > A[p + 1]:
            peak_debug.append(A[p])
            if len(peak) == 0:
                peak.append(0)
            else:
                peak.append(p - last_peak + peak[len(peak) -1]) # Prefix Sum of Peak Distances
            last_peak = p
    if (len(peak) == 0):
        return 0
    if (len(peak) == 1):
        return 1
    D = peak[-1]
    max_k = int(math.sqrt(D))
    while max_k * (max_k +1) <= D:
        max_k += 1
    M = len(peak)
    max_k = min(max_k, M)
    #print A
    #print peak
    #print peak_debug
    #print D

    for k in xrange(max_k,0 ,-1):
        #print "k =", k
        for addition in xrange(0,1):
            bin_index = -1
            bin_count = 1
            first_in_last_bin = 0
            first_in_this_bin = 0

            for i in xrange(0,M):
                this_bin = (peak[i] + addition) // k
                #print "peak", i, "in bin", this_bin
                if this_bin > bin_index:
                    #print "new bin", this_bin, "found"
                    first_in_this_bin = peak[i]
                    bin_index = this_bin
                if peak[i] - first_in_last_bin >=k:
                    #print "bin", this_bin, "counted"
                    bin_count += 1
                    first_in_last_bin = peak[i]
                if bin_count >= k:
                    return k
                
    return 0
    pass        


import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class CorrectnessTest(Test):
    def test_example(self):
        A = [1, 5, 3, 4, 3, 4, 1, 2, 3, 4,  6,  2]
        A = [1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2] 
        print A
        actual = solution(A)
        expected = naive(A)
        self.assertEqual(actual, expected)
        pass
    def test_single(self):
        A = [1]
        print A
        actual = solution(A)
        expected = 0
        self.assertEqual(actual, expected)
        pass
    def test_double(self):
        A = [10,2]
        print A
        actual = solution(A)
        expected = 0
        self.assertEqual(actual, expected)
        pass
    def test_one(self):
        A = [1, 5, 3]
        print A
        actual = solution(A)
        expected = 1
        self.assertEqual(actual, expected)
        pass
    def test_double1(self):
        A = [1, 5, 3,10,2]
        print A
        actual = solution(A)
        expected = 2
        self.assertEqual(actual, expected)
        pass

    def test_three_neg(self):
        A = [1, 5, 3,10,2,11,1]
        print A
        actual = solution(A)
        expected = 2
        self.assertEqual(actual, expected)
        pass

    def test_three_pos(self):
        A = [1, 5, 3,2,10,2,7,11,1]
        print A
        actual = solution(A)
        expected = 3
        self.assertEqual(actual, expected)
        pass

    def test_no_peak(self):
        A = [5,5,5,5,5,5,5,5,5,5]
        print A
        actual = solution(A)
        expected = 0
        self.assertEqual(actual, expected)
        pass

    def test_two_small(self):
        A = [5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10]
        print A
        actual = solution(A)
        expected = 4
        self.assertEqual(actual, expected)
        pass
    def test_ten1(self):
        A = [19, 1, 14, 15, 25, 11, 21, 6, 18, 38, 37, 7, 10, 4, 35, 33, 5, 39, 17, 0, 12, 24, 9, 31, 28, 34, 32, 13, 27, 22, 26, 23, 2, 3, 29, 30, 20, 8, 16, 36]
        print A
        actual = solution(A)
        expected = naive(A)
        self.assertEqual(actual, expected)
        pass

class LoadTest(Test):
    def __init__(self, methodName='runTest', n=None):
        super(LoadTest, self).__init__(methodName)
        self._N = n

    def setUp(self):
        super(LoadTest,self).setUp()
        self._A = random.sample([x for x in xrange(self._N)],self._N)
        print self._N
        #print self._A
        #print self._B

    def tearDown(self):
        super(LoadTest, self).tearDown()

    def load_test(self):
        A = self._A
        actual = solution(A)
        expected = naive(A)
        self.assertTrue(actual == expected)
        pass

def runload():
    step = 1
    runTimeAll = []
    failTests = 0
    for i in xrange(1,1000):
        testCase = LoadTest("load_test", n = 1000)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def customload():
    suite = unittest.TestLoader().loadTestsFromTestCase(LoadTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)
    pass

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(CorrectnessTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)

def runone(test):
     unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(test))

if __name__ == "__main__":
    runall()

