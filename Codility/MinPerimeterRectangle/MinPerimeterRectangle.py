﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

from CodilityLib import Test

import Queue
import math
def solution(N):
    sqrt_n = math.sqrt(N)
    M = int(N // sqrt_n)
    for i in xrange (M,0,-1):
        if N % i == 0:
            return 2 * (i + (N // i))
    return 2 * (N+1)
            

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class CorrectnessTest(Test):
    def test_example(self):
        A = 30
        print A
        actual = solution(A)
        expected = 22
        self.assertEqual(actual, expected)
        pass
    def test_single(self):
        A = 1
        print A
        actual = solution(A)
        expected = 4
        self.assertEqual(actual, expected)
        pass
    def test_square(self):
        A = 4
        print A
        actual = solution(A)
        expected = 8
        self.assertEqual(actual, expected)
        pass
    def test_double(self):
        A = 2
        print A
        actual = solution(A)
        expected = 6
        self.assertEqual(actual, expected)
        pass

    def test_small(self):
        A = 3
        print A
        actual = solution(A)
        expected = 8
        self.assertEqual(actual, expected)
        pass

class LoadTest(Test):
    def __init__(self, methodName='runTest', n=None):
        super(LoadTest, self).__init__(methodName)
        self._N = n

    def setUp(self):
        super(LoadTest,self).setUp()
        self._A = random.sample([x for x in xrange(self._N)],self._N)
        print self._N
        #print self._A
        #print self._B

    def tearDown(self):
        super(LoadTest, self).tearDown()

    def load_test(self):
        A = self._N
        actual = solution(A)
        expected = -1
        self.assertGreater(actual, expected)
        pass

def runload():
    step = 1
    runTimeAll = []
    failTests = 0
    for i in xrange(1,1000):
        testCase = LoadTest("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def customload():
    suite = unittest.TestLoader().loadTestsFromTestCase(LoadTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)
    pass

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(CorrectnessTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)

def runone(test):
     unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(test))

if __name__ == "__main__":
    runall()

