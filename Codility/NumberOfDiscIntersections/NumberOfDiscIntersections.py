﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

import CodilityLib
import copy

MAX_N = 100000

def distance(A):
    # return array D,S
    # Di = Ai + A0 - i
    # Di >= 0 <-> Disc 0 and Disc i intersect
    # Si = Ai - A0
    N = len(A)
    D = [0] * N
    S = [0] * N
    for i in xrange(0,N):
        if A[i] > MAX_N:      
            A[i] = MAX_N

        D[i] = A[i] + A[0] - i
        S[i] = A[i] - A[0]
    return D,S



def counting(D,S):
    # return array C
    # Ci = number of element of D has value >= Si
    # D, S has equal length
    N = len(D)
    Dsorted = copy.copy(D)
    Dsorted.sort()
    Ssorted = copy.copy(S)
    Ssorted.sort()
    for i in xrange(


def findJump(D,left,right,val):
    #print left,right, val
    # Given sorted array D, find index of the last element that smaller than
    # val
    # By that we can interpolate how many elements in the range greater than or
    # equal val
    # Return -1 if cannot find one
    if D[left] >= val:
        return left
    if D[right] < val:
        return -1
    mid = (right + left) / 2
    if D[mid] > val:
        return findJump(D,left,mid,val)
    else:
        return findJump(D,mid + 1,right,val)


def solution(A):
    N = len(A)
    maxN = 100000
    for j in xrange (0,N):
        D = [0] * (N)
        for i in xrange(j,N):
            if A[i] > maxN: 
                A[i] = maxN
            D[i] = A[i] + A[j] - (i-j)
        print D

def solution1(A):
    N = len(A)
    Dzero = [0] * N
    maxN = 100000
    R = 0
    for i in xrange(0,N):
        if A[i] > maxN:      
            A[i] = maxN
        Dzero[i] = A[i] + A[0] - i
        if Dzero[i] >= 0:
            R += 1
    #print Dzero
    D = copy.copy(Dzero)
    D.sort()
    #print D
    #print R
    # Ak + Aj - (j-k) = (Ak - A0) + (Aj + A0 -j) + k >= 0 <-> A0 - Ak -k <= Dj
    # <-> val <=Dj <-> count #elem in D which larger than A0 - Ak - k then two
    # k = 4, j = 0, Ak = 1, Aj = 1 Ak+Aj-(j-k) = 1+1-(1-4) = 1 but two disc won't intersect
    # discs intersect
    for i in xrange(1,N):
        D.remove(Dzero[i-1])
        #print D
        ND = len(D)
        Ri = findJump(D,0,ND - 1, A[0] - A[i] - i)
        if Ri > -1:
            R += (ND) - Ri
        #print Ri, ND - Ri
    #print R - N
    return R - N

    

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):
    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime = self._tock - self._tick
        return runTime.microseconds / 1000

class CorrectnessTest(Test):
    def test_example(self):
        A = [1,5,2,1,4,0]
        #A = [100,100,100,100,100]
        #print A
        actual = solution(A)
        expected = 11
        self.assertEqual(actual,expected)
        pass

class LoadTest(Test):

    def __init__(self, methodName='runTest', n=None):
        super(LoadTest, self).__init__(methodName)
        self._n = n

    def setUp(self):
        super(LoadTest,self).setUp()

    def tearDown(self):
        super(LoadTest, self).tearDown()

    def load_test(self):
        pass

def runload():
    step = 10
    runTimeAll = []
    for i in xrange(1,1000):
        testCase = LoadTest("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    print runTimeAll
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def customload():
    suite = unittest.TestLoader().loadTestsFromTestCase(LoadTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)
    pass

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(CorrectnessTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)

if __name__ == "__main__":
    runall()
