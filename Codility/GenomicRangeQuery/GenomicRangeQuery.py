﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

import CodilityLib

GENOME = {
    'A': 1,
    'C': 2,
    'G': 3,
    'T': 4
    }

L = len(GENOME)

def counting(A): 
    N = len(A) 
    # Matrix of N cols and 4 rows
    count = [[0 for x in range(N)] for x in range(L)] 
    for k in xrange(N):
        if k > 0:
            for j in xrange(L):
                count[j][k] = count[j][k - 1]
        count[GENOME[A[k]] - 1][k] +=1
    return count

def solution(S,P,Q):
    #P begin range indices
    #Q end range indices
    M = len(P)
    N = len(S)
    R = [-1] * M
    # Array of prefix sum for diff for a range
    #D = [0] * L
    # Count
    count = counting(S)
    for i in xrange(M):
        for j in xrange(L):
            leftCount = 0
            if P[i] > 0:
                leftCount = count[j][P[i] - 1]
            diff = count[j][Q[i]] - leftCount
            #D[j] = diff
            if diff > 0:
                R[i] = j + 1
                break
        #print D
    return R

    

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):
    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime = self._tock - self._tick
        return runTime.microseconds / 1000

class CorrectnessTest(Test):
    def test_example(self):
        S = 'CAGCCTA'
        P = [2, 5, 0]
        Q = [4, 5, 6]
        actual = solution(S,P,Q)
        expected = [2, 4, 1]
        self.assertEqual(actual,expected)

    def test_single(self):
        S = 'C'
        P = [0]
        Q = [0]
        actual = solution(S,P,Q)
        expected = [2]
        self.assertEqual(actual,expected)
        pass

    def test_single_many(self):
        S = 'A'
        P = [0,0,0,0,0]
        Q = [0,0,0,0,0]
        actual = solution(S,P,Q)
        expected = [1,1,1,1,1]
        self.assertEqual(actual,expected)
        pass

    def test_small_separate(self):
        S = 'T' * 12
        P = [0,2,4,6,8,10]
        Q = [1,3,5,7,9,11]
        actual = solution(S,P,Q)
        expected = [4] * 6
        self.assertEqual(actual,expected)
        pass

class LoadTest(Test):

    def __init__(self, methodName='runTest', n=None):
        super(LoadTest, self).__init__(methodName)
        self._n = n

    def setUp(self):
        if self._testMethodName == 'test_max_extreme':
            self._S = ''
            for i in xrange(100000):
                self._S += GENOME.keys()[random.randint(0,L-1)]
            self._P = [0] * 50000
            self._Q = [0] * 50000
            for i in xrange(50000):
                self._P[i] = i
                self._Q[i] = i + 50000
            self._R = [0] * 50000

        if self._testMethodName == 'test_max_NM':
            self._S = ''
            for i in xrange(100000):
                self._S += GENOME.keys()[random.randint(0,L-1)]
            self._P = [0] * 50000
            self._Q = [100000 - 1] * 50000
            for i in xrange(50000):
                self._P[i] = i
                self._Q[i] = i + 50000
            self._R = [1] * 50000
        super(LoadTest,self).setUp()

    def tearDown(self):
        super(LoadTest, self).tearDown()

    def test_max_extreme(self):
        actual = solution(self._S,self._P, self._Q)
        self.assertGreater(actual, self._R)
        pass

    def test_max_NM(self):
        actual = solution(self._S,self._P, self._Q)
        self.assertEqual(actual, self._R)
        pass 

    def load_test(self):
        pass

def runload():
    step = 10
    runTimeAll = []
    for i in xrange(1,1000):
        testCase = LoadTest("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    print runTimeAll
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def customload():
    suite = unittest.TestLoader().loadTestsFromTestCase(LoadTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)
    pass

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(CorrectnessTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)

if __name__ == "__main__":
    runall()
