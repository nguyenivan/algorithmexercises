﻿def solution(X,Y,D):
    ret = (Y - X + (D - 1)) / D
    return ret

import unittest
import random
import numpy as np
import os

class Test(unittest.TestCase):
    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None


    def test_example(self):
        X = 10
        Y = 85
        D = 30
        actual = solution(X,Y,D)
        expected = 3
        self.assertEqual(actual,expected)

    def test_one(self):
        X = 10
        Y = 40
        D = 30
        actual = solution(X,Y,D)
        expected = 1
        self.assertEqual(actual,expected)

    def test_zero(self):
        X = 10
        Y = 10
        D = 30
        actual = solution(X,Y,D)
        expected = 0
        self.assertEqual(actual,expected)

    def test_edge1(self):
        X = 1
        Y = 1
        D = 1
        actual = solution(X,Y,D)
        expected = 0
        self.assertEqual(actual,expected)

    def test_edge2(self):
        X = 1000000000
        Y = 1000000000
        D = 1000000000
        actual = solution(X,Y,D)
        expected = 0
        self.assertEqual(actual,expected)
        
    def test_max1(self):
        X = 1
        Y = 1000000000
        D = 1
        actual = solution(X,Y,D)
        expected = 999999999
        self.assertEqual(actual,expected)

    def test_max2(self):
        X = 1
        Y = 1000000000
        D = 999999999
        actual = solution(X,Y,D)
        expected = 1
        self.assertEqual(actual,expected)
    #def test_correctness(self):
    #    A = [-1000,1000]
    #    actual = solution(A)
    #    expected = 2000
    #    self.assertEqual(actual,expected)

    #def test_small_range(self):
    #    #A = np.loadtxt("small_range.txt", int)
    #    A = self._A
    #    actual = solution(A)
    #    expected = 7
    #    self.assertEqual(actual,expected)

    #def test_medium_random1(self):
    #    #A = np.loadtxt("medium_random1.txt", int)
    #    A = self._A
    #    actual = solution(A)
    #    expected = 0
    #    self.assertEqual(actual,expected)

    #def test_large_extreme(self):
    #    A = self._A
    #    #A = np.loadtxt("large_extreme.txt", int)
    #    actual = solution(A)
    #    expected = 0
    #    self.assertEqual(actual,expected)
def runtest():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(Test(s))
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity = 2).run(suite)

if __name__ == "__main__":
    runtest()