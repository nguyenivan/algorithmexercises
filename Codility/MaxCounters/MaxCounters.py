﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

import CodilityLib

def solution(N,A):
    counters = [0] * N
    M = len(A)
    maxcounter = 0
    maxvalue = 0
    for k in xrange(M): 
        if (A[k] <= N):
            if (counters[A[k]-1] < maxcounter):
                counters[A[k]-1] = maxcounter + 1
            else:
                counters[A[k]-1] += 1
            if maxvalue < counters[A[k]-1]:
                maxvalue = counters[A[k]-1]
        else:
            maxcounter = maxvalue
    
    for j in xrange(N):
        if counters[j] < maxcounter:
            counters[j] = maxcounter  
    return counters

    

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):

    def __init__(self, methodName='runTest', n=None):
        super(Test, self).__init__(methodName)
        self._n = n

    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime = self._tock - self._tick
        return runTime.microseconds / 1000

    def test_example(self):
        A = [3, 4, 4, 6, 1, 4, 4 ]
        N = 5
        actual = solution(N,A)
        expected = [3, 2, 2, 4, 2]
        self.assertEqual(actual,expected)

    def test_single(self):
        A = [1]
        N = 5
        actual = solution(N,A)
        expected = [1, 0, 0, 0]
        self.assertEqual(actual,expected)

    def load_test(self):
        M = self._n
        N = random.randint(100,100000)
        A = random.sample([i for i in xrange(1,N+2)] * 10, M)
        actual = solution(N, A)
        #print 'N =', N, ", M = ", M, ", solution = ", actual
        print 'N =', N, ", M = ", M
        self.assertEqual(len(actual), N)
    

def runload():
    step = 10
    runTimeAll = []
    for i in xrange(1,1000):
        testCase = Test("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    print runTimeAll
    #plt.plot(runTimeAll, "ro")
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(Test(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity = 2).run(suite)

if __name__ == "__main__":
    runall()
    #runload()