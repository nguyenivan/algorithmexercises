﻿def solution(A):
    N = len(A)
    expTotal = (N+2)*(N+1)/2
    #actTotal = sum(A); ret = expTotal - actTotal
    ret = expTotal
    for i in A:
        ret = ret - i
    return ret

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):

    def __init__(self, methodName='runTest', n=None):
        super(Test, self).__init__(methodName)
        self._n = n

    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime =  self._tock - self._tick
        return runTime.microseconds / 1000

    def test_example(self):
        A = [2 ,3, 1, 5]
        actual = solution(A)
        expected = 4
        self.assertEqual(actual,expected)

    def test_20(self):
        A = [16, 2, 7, 18, 4, 8, 20, 6, 3, 12, 15, 13, 1, 19, 17, 11, 14, 9, 5, 21]
        actual = solution(A)
        expected = 10
        self.assertEqual(actual,expected)

    def test_extreme(self):
        A = self._A
        actual = solution(A)
        expected = 44677L
        self.assertEqual(actual,expected)

    def load_test(self):
        N = self._n
        print 'N =', N
        A = random.sample(xrange(1, N + 2), N)
        actual = solution(A)
        min = 0
        self.assertGreater(actual, min)
    

def runload():
    step = 10
    runTimeAll = []
    for i in xrange(0,10000):
        testCase = Test("load_test", n = i*step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    print runTimeAll
    #plt.plot(runTimeAll, "ro")
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def runtest():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(Test(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity = 2).run(suite)


if __name__ == "__main__":
    runtest()
    runload()