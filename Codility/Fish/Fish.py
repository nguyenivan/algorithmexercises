﻿import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

import Queue

def solution(A, B):
    #print 
    #print A
    #print B
    N = len (A)
    queue = Queue.LifoQueue(N)
    R = 0
    for i in xrange(N):
        # If fish flowing downstream, queue it
        q = A[i]
        if B[i]:
            queue.put(q)
            #print "Fish size", q, "queued."
        elif queue.empty():
            #print "Fish size", q, "escaped."
            R += 1
        else:
            while not queue.empty():
                p = int(queue.get())
                if p < q:
                    #print "Fish size", p, " in queue is eaten by fish size", q
                    pass
                else:
                    #print "Fish size", q, " in is eaten by fish size", p, "in queue."
                    queue.put(p)
                    break
            if queue.empty():
                #print "Fish size", q, "escaped."
                R += 1
    #final = []
    #while not queue.empty():
    #    final.append(queue.get())
    #print "Final queue:", final
    #print "Final queue size: ", len(final), "Fish that escaped: ", R
    return queue.qsize() + R

    pass

def solution_naive    (A,B):
    # Naive solution
    #print
    #print A
    #print B
    N = len (A)
    F = [1] * N #position
    for i in xrange(N):
        for p in xrange (N-1):
            if F[p] == 1:
                for q in xrange(p+1, N):
                    if  F[q] == 1:
                        if B[p] == 1 and B[q] == 0 :
                            if A[p] > A[q]:
                                #print "Fish size", A[q], "will eventually be eaten by fish size", A[p]
                                F[q] = 0
                            elif A[p] < A[q]:
                                #print "Fish size", A[p], "will eventually be eaten by fish size", A[q]
                                F[p] = 0
                        break
    #print "Final pass:", F

    return sum(F)

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):
    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime = self._tock - self._tick
        return runTime.microseconds / 1000

class CorrectnessTest(Test):
    def test_example(self):
        A = [4,3,2,1,5]
        B = [0,1,0,0,0]
        actual = solution(A,B)
        expected = 2
        self.assertEqual(actual,expected)
        pass
    def test_extreme_small1(self):
        A = [4]
        B = [0]
        actual = solution(A,B)
        expected = 1
        self.assertEqual(actual,expected)
        pass
    def test_extreme_small2(self):
        A = [4]
        B = [1]
        actual = solution(A,B)
        expected = 1
        self.assertEqual(actual,expected)
        pass
    def test_extreme_small3(self):
        A = [4,1]
        B = [0,0]
        actual = solution(A,B)
        expected = 2
        self.assertEqual(actual,expected)
        pass
    def test_extreme_small4(self):
        A = [4,1]
        B = [0,1]
        actual = solution(A,B)
        expected = 2
        self.assertEqual(actual,expected)
        pass
    def test_extreme_small5(self):
        A = [4,1]
        B = [1,0]
        actual = solution(A,B)
        expected = 1
        self.assertEqual(actual,expected)
        pass
    def test_extreme_small6(self):
        A = [4,1]
        B = [1,1]
        actual = solution(A,B)
        expected = 2
        self.assertEqual(actual,expected)
        pass

    def test_random_20(self):
        A = [11, 6, 10, 7, 1, 14, 4, 2, 13, 9, 3, 5, 8, 0, 12]
        B = [1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1]
        actual = solution(A,B)
        expected = solution_naive(A,B)
        self.assertEqual(actual,expected)
        pass

class LoadTest(Test):

    def __init__(self, methodName='runTest', n=None):
        super(LoadTest, self).__init__(methodName)
        self._N = n

    def setUp(self):
        super(LoadTest,self).setUp()
        self._A = random.sample([x for x in xrange(self._N)],self._N)
        self._B = random.sample([0,1] * self._N, self._N)
        print self._N
        #print self._A
        #print self._B

    def tearDown(self):
        super(LoadTest, self).tearDown()

    def load_test(self):
        A = self._A 
        B = self._B
        actual = solution(A, B)
        #expected = solution_naive(A, B)
        expected = -1
        self.assertGreater(actual, expected)
        pass

def runload():
    step = 10
    runTimeAll = []
    failTests = 0
    for i in xrange(1,1000):
        testCase = LoadTest("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def customload():
    suite = unittest.TestLoader().loadTestsFromTestCase(LoadTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)
    pass

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(CorrectnessTest)
    unittest.TextTestRunner(verbosity = 2).run(suite)

def runone(test):
     unittest.TextTestRunner(verbosity=2).run(CorrectnessTest(test))

if __name__ == "__main__":
    runall()
