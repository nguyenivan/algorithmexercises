import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), '.'))

import CodilityLib

def solution(X,A):
    count = [0] * X
    n = len(A)
    if X > n:
        return -1
    total = 0
    for i in xrange(0, n):
        if count[A[i] - 1]:
            continue
        total = total + 1
        if total == X:
            return i
        count[A[i] - 1] = 1
    
    return -1

    

import unittest
import random
import numpy as np
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Test(unittest.TestCase):

    def __init__(self, methodName='runTest', n=None):
        super(Test, self).__init__(methodName)
        self._n = n

    def setUp(self):
        data = self._testMethodName[len("test_"):] + ".txt"
        if  os.path.exists(data):
            self._A = np.loadtxt(data, int)
        else:
            self._A = None

        self._tick = datetime.now()

    def tearDown(self):
        self._tock = datetime.now()
    
    def getRunTime(self):
        runTime = self._tock - self._tick
        return runTime.microseconds / 1000

    def test_example(self):
        A = [1, 3, 1, 4, 2, 3, 5, 4]
        X = 5
        actual = solution(X,A)
        expected = 6
        self.assertEqual(actual,expected)

    def test_min_positive(self):
        A = [1]
        X = 1
        actual = solution(X,A)
        expected = 0
        self.assertEqual(actual,expected)

    def test_min_negative(self):
        A = [1]
        X = 3
        actual = solution(X,A)
        expected = -1
        self.assertEqual(actual,expected)

    def test_20(self):
        A = [3, 4, 1, 2, 2, 1, 5, 7, 8, 3, 4, 5, 6, 7, 2, 4, 8, 8, 1, 3]
        X = 8
        actual = solution(X,A)
        expected = 12
        self.assertEqual(actual,expected)

    def test_increase(self):
        X = 100
        A = [i for i in xrange(1,X + 1)]
        actual = solution(X,A)
        expected = X - 1
        self.assertEqual(actual,expected)

    def test_decrease(self):
        X = 100
        A = [i for i in xrange(1,X + 1)]
        A.reverse()
        actual = solution(X,A)
        expected = X - 1
        self.assertEqual(actual,expected)

    def test_random_negative(self):
        N = random.randint(1000,2000)
        X = random.randint(N+1, N+1000)
        A = random.sample([i for i in xrange(1,X+1)], N)
        actual = solution(X,A)
        expected = - 1
        self.assertEqual(actual,expected)


    def load_test(self):
        N = self._n
        X = random.randint(1,N / 2)
        A = random.sample([i for i in xrange(1, X+1)] * (1 + N/X), N)
        actual = solution(X, A)
        min = -1
        #print "A = ", A
        print 'N =', N, ", X = ", X, ", solution = ", actual
        self.assertGreaterEqual(actual, min)
    

def runload():
    step = 10
    runTimeAll = []
    for i in xrange(1,1000):
        testCase = Test("load_test", n = i * step)
        testResult = unittest.TextTestRunner(verbosity =2).run(testCase) 
        runTimeAll.append(testCase.getRunTime())
    print runTimeAll
    #plt.plot(runTimeAll, "ro")
    plt.plot(runTimeAll)
    plt.ylabel('Run Time (ms)')
    plt.xlabel('N (x %s)' % step)
    plt.show()

def runeach():
    testNames = unittest.TestLoader().getTestCaseNames(Test)
    for s in testNames:
        unittest.TextTestRunner(verbosity=2).run(Test(s))

def runall():
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity = 2).run(suite)

if __name__ == "__main__":
    runall()
    #runload()